import { Injectable } from '@angular/core';
import { Food } from '../models/food.model';

@Injectable({
  providedIn: 'root',
})
export class FoodService {
  getFoods(): Food[] {
    return [
      {
        id: 1,
        title: 'Sea Food',
        price: 250,
        image: 'assets/images/foods/seafood-dishes.png',
        description:
          'Sea foods you should limit your intake in order to avoid worsening your health. The first food we want to mention in this list is calamari or squid.',
      },
      {
        id: 2,
        title: 'Hamburger',
        price: 80,
        image: 'assets/images/foods/hamburger.png',
        description:
          'A hamburger (also burger for short) is a food, typically considered a sandwich, consisting of one or more cooked patties of ground meat, usually beef, placed inside a sliced bread roll or bun.',
      },
      {
        id: 3,
        title: 'Mussels',
        price: 300,
        image: 'assets/images/foods/mussel.png',
        description:
          'Mussels are topped with a rich curry butter, sealed in foil packets, and grilled until done in this terrific recipe. They are so delicious! I serve these as an appetizer with warm crusty bread for dunking followed by a big green salad topped with grilled chicken or shrimp for an easy summertime meal.',
      },
      {
        id: 4,
        title: 'Pizza',
        price: 279,
        image: 'assets/images/foods/pizza.png',
        description:
          'Pizza is a favorite food for many around the world. The addicting combination of delicious crust, sweet tomato sauce and salty mozzarella cheese is sure to please even the pickiest of eaters. However, it’s commonly labeled unhealthy, as it can be high in calories, sodium and carbs.',
      },
      {
        id: 5,
        title: 'Breakfast',
        price: 150,
        image: 'assets/images/foods/scott-ish-breakfast.png',
        description:
          'A breakfast burrito on a plate with glass ramekins of salsa and sour cream Breakfast Burritos A shallow white dish with a scoop of homemade granola with dried cranberries',
      },
      {
        id: 6,
        title: 'Tambi',
        price: 130,
        image: 'assets/images/foods/tambi.png',
        description:
          ' Tambi is done when the syrup is all dried up and the vermicelli is soft Garnish with almonds.',
      },
      {
        id: 7,
        title: 'Brown Sugar Pearl',
        price: 75,
        image: 'assets/images/foods/brown sugar pearl.jpg',
        description:
          'They are soft, chewy, translucent and the flavour works so well with milk or milk tea… or whatever else you put them in. ',
      },
      {
        id: 8,
        title: 'Cookies and Cream',
        price: 75,
        image: 'assets/images/foods/Cookies and Cream.jpg',
        description:
          'Oreo Cookies and Cream Milk Tea Powder by MilkTea Mate is made of original Oreo cookies and cream ingredients that suits well when mixed with a tea base. It has a creamy texture that your customers will surely love.',
      },
      {
        id: 9,
        title: 'Mango Graham',
        price: 75,
        image: 'assets/images/foods/Mango Graham.jpg',
        description:
          'Mango Graham float is a proud Philippine dessert, a very easy yet highly presentable-looking cold treat to make your family feel extra special. ',
      },
      {
        id: 10,
        title: 'Okinawa',
        price: 75,
        image: 'assets/images/foods/Okinawa.jpg',
        description:
          'Okinawa tea is a different kind of milk tea, it’s a blend of tea leaves, milk, and sugar. This sort of milk tea gets its name from Okinawa Prefecture in Japan. Okinawa milk tea can be delighted in hot, or served frosted all alone or with Tapioca pearls',
      },
      {
        id: 11,
        title: 'Oreo Cream Cheese',
        price: 75,
        image: 'assets/images/foods/Oreo Cream Cheese.jpg',
        description:
          ' Oreo Cream Cheese has become my go-to drink, and it’s the reason for my love for this millennial drink.',
      },
      {
        id: 12,
        title: 'Red Velvet',
        price: 75,
        image: 'assets/images/foods/Red Velvet.jpg',
        description:
          'Red Velvet Milk Tea is made of pure quality red velvet ingredients that suits well when mixed with a tea base. It has a creamy texture that your customers will surely love.',
      },
      {
        id: 13,
        title: 'Strawberry',
        price: 75,
        image: 'assets/images/foods/Strawberry.jpg',
        description:
          'Traditionally, strawberry tea is used to treat diarrhea and is applied externally to ease inflammatory skin disorders and joint pain.',
      },
      {
        id: 14,
        title: 'Taro',
        price: 75,
        image: 'assets/images/foods/Taro.jpg',
        description:
          ' Taro milk tea is a truly delicious variation that has more creamy and unctuous body thanks to the addition of starchy taro root.',
      },
      {
        id: 15,
        title: 'Wintermelon',
        price: 75,
        image: 'assets/images/foods/Wintermelon.jpg',
        description:
          'Winter melon is the most confusing drink we have, as a lot of customers come in and think they are getting watermelon flavour when they order it.',
      },
      
    ];
  }

  getFood(id: number): Food {
    return this.getFoods().find((food) => food.id === id);
  }
}
